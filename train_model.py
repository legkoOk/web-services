import numpy as np
from sklearn import datasets, neighbors
import joblib

iris = datasets.load_iris()
iris_X = iris.data
iris_y = iris.target

clf = neighbors.KNeighborsClassifier(3, weights='uniform')
clf.fit(iris_X, iris_y)

joblib.dump(clf,'clf.pkl')


