from flask import Flask,request, jsonify, abort, redirect, url_for, render_template, send_file
import numpy as np
import joblib
# from requests import request
app = Flask(__name__)
app.config.update(dict(
    SECRET_KEY="powerful secretkey",
    WTF_CSRF_SECRET_KEY="a csrf secret key"
))

clf = joblib.load('clf.pkl')


@app.route('/')
def hello_world():
    print(1+2)
    return '<h1>Hello, my best litle friend!!!</h1>'

@app.route('/user/<username>')
def show_user_profile(username):
    # show the user profile for that user
    return 'User %s' % username

@app.route('/avg/<nums>')
def avg(nums):
    # # show the user profile for that user
    nums = nums.split(',')
    nums = list(map((lambda x: float(x)),nums))
    avg_res = sum(nums)/len(nums)
    print(nums)
    return str(avg_res)

@app.route('/iris/<param>')
def iris(param):
    nums = param.split(',')
    nums = np.array(list(map((lambda x: float(x)),nums)))
    print(nums)
    
    y_pred = clf.predict(nums.reshape(1,-1))

    return str(y_pred)

@app.route('/show_image')
def show_image():
    return '<img src="/static/setosa.jpg" alt="Italian Trulli">'

@app.route('/badrequest400')
def bad_request():
    return abort(400)

@app.route('/iris_post', methods=['POST'])
def add_message():

    try:
        content = request.get_json()

        nums = content["flower"].split(',')
        nums = np.array(list(map((lambda x: float(x)),nums)))
        print(nums)
        
        y_pred = {"class":str(clf.predict(nums.reshape(1,-1)))}
    except:
        return redirect(url_for('bad_request'))

    return jsonify(y_pred)

from flask_wtf import FlaskForm
from wtforms import StringField, FileField
from wtforms.validators import DataRequired
from werkzeug.utils import secure_filename
import os
import pandas as pd

class MyForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    file = FileField()

@app.route('/submit', methods=('GET', 'POST'))
def submit():
    form = MyForm()
    if form.validate_on_submit():
        f = form.file.data
        filename = form.name.data + '.csv'#'upload.txt'#secure_filename(f.filename)
        # f.save(os.path.join(
        #     filename
        # ))
        df = pd.read_csv(f, header=None)
        # print(df.head())
        predict = clf.predict(df)
        result = pd.DataFrame(predict)
        result.to_csv(filename, index=False)

        return send_file(filename,
                    mimetype='text/csv',
                    attachment_filename=filename,
                    as_attachment=True)

        return 'form submitted!'
    return render_template('submit.html', form=form)

import os
from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = ''
ALLOWED_EXTENSIONS = {'csv','txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = file.filename + '_upload'
            file.save(filename)
            return 'file_uploaded'
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''