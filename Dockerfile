FROM python:3.6-slim

COPY . .

WORKDIR /root

RUN pip install flask gunicorn numpy sklearn scipy joblib requests flask_wtf wtforms pandas